import Vue from 'vue';
import Vuelidate from 'vuelidate';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import WebFont from 'webfontloader';
import App from './App.vue';

import './style/main.scss';

WebFont.load({
  google: {
    families: ['Noto Sans'],
  },
});

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(BootstrapVueIcons);

Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');
